﻿using System;
namespace BLEBeaconFramework.Interfaces
{
    public enum Proximity {
        unknown,
        immediate,      // high certainty closer than 'near'
        near,           // 1 - 3 meters
        far             // further than 'near'
    }
    public interface IBeacon
    {
        string Uuid { get; set; }
		int Minor { get; set; }
		int Major { get; set; }
        int Rssi { get; set; }
        Proximity Proximity { get; set; }
        double Distance { get; set; }
		double Accuracy { get; set; }

        void Calibrate();
	}
}