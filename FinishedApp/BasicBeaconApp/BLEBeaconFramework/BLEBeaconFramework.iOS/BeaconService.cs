﻿using System;
using Foundation;
using CoreBluetooth;
using CoreLocation;
using CoreFoundation;
using BLEBeaconFramework.Interfaces;
using System.Collections.Generic;
using BLEBeaconFramework.iOS;
using System.Diagnostics;

//[assembly: Xamarin.Forms.Dependency(typeof(BeaconService))]
namespace BLEBeaconFramework.iOS
{
    public class BeaconService : IBeaconService
    {
        private bool trackingBeacons = false;
        CLLocationManager beaconLocationMgr;
		CLBeaconRegion beaconRegion;
		
		public BeaconService()
        {
        }

        #region BeaconService Interfaces
        public event EventHandler<BeaconsSitedEventArgs> BeaconsSited = delegate { };
        public event EventHandler BeaconServiceConnected;

        public void Initialise()
        {
            var myUUID = new NSUuid(Settings.Uuid);
            beaconRegion = new CLBeaconRegion(myUUID, Settings.BeaconAppId);

            beaconRegion.NotifyEntryStateOnDisplay = true;
            beaconRegion.NotifyOnEntry = true;
            beaconRegion.NotifyOnExit = true;

            beaconLocationMgr = new CLLocationManager();
            beaconLocationMgr.RequestAlwaysAuthorization();

            // OTHER OPTIONS:
            //          beaconLocationMgr.DistanceFilter = 99999;
            //          beaconLocationMgr.RequestWhenInUseAuthorization ();

            // EVENTS:
            beaconLocationMgr.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
            {
                ProcessBeaconUpdates(e);
            };

            // OTHER EVENTS:
            //          beaconLocationMgr.RegionEntered += (object sender, CLRegionEventArgs e) => {
            //              if (SharedValues.SharedObject.TestingMode)
            //              Console.WriteLine("RegionEntered triggered");
            //              if (e.Region.Identifier == appId) {  // This is how to distinguish between apps and use the same beacons
            //              }
            //          };
            //
            //          beaconLocationMgr.RegionLeft += (object sender, CLRegionEventArgs e) => {
            //              if (SharedValues.SharedObject.TestingMode)
            //              Console.WriteLine("RegionLeft triggered");
            //          };
            //
            //          beaconLocationMgr.DidDetermineState += (object sender, CLRegionStateDeterminedEventArgs e) => {
            //              if (SharedValues.SharedObject.TestingMode)
            //              Console.WriteLine("DidDetermineState triggered");
            //          };
            //
            //          beaconLocationMgr.DidVisit += (object sender, CLVisitedEventArgs e) => {
            //              if (SharedValues.SharedObject.TestingMode)
            //              Console.WriteLine("DidVisit triggered");
            //          };

            BeaconServiceConnected(this, new EventArgs { });

        }

        public void StartTracking()
        {
            if (!trackingBeacons)
            {
                if (beaconLocationMgr != null)
                {
                    beaconLocationMgr.StartMonitoring(beaconRegion);
                    beaconLocationMgr.StartRangingBeacons(beaconRegion);

                }
                else
                {
                    throw new Exception("Need to initilise beacon manager");
                }

                trackingBeacons = true;
            }
        }

        public void StopTracking()
        {
			if (trackingBeacons)
			{
				if (beaconLocationMgr != null)
				{
					beaconLocationMgr.StopMonitoring(beaconRegion);
					beaconLocationMgr.StopRangingBeacons(beaconRegion);
				}

				trackingBeacons = false;
			}
		}
		#endregion

		#region private method
		/// <summary>
		/// Processes the beacon updates.
		/// </summary>
		/// <param name="e">iOS Beacon event args</param>
		private void ProcessBeaconUpdates(CLRegionBeaconsRangedEventArgs e)
		{
			if (e.Beacons.Length == 0)
				return;

            var beacons = new List<IBeacon>();
            foreach (var clBeacon in e.Beacons)
            {
                Debug.WriteLine("Minor: {0}, Rssi: {1}", clBeacon.Minor, clBeacon.Rssi);

                IBeacon beacon = new Beacon();

                beacon.Uuid = clBeacon.ProximityUuid.ToString();
                beacon.Major = Int32.Parse(clBeacon.Major.ToString());
                beacon.Minor = Int32.Parse(clBeacon.Minor.ToString());
                beacon.Rssi = (int)clBeacon.Rssi;
                beacon.Distance = 0.0;  //TODO

                beacon.Accuracy = clBeacon.Accuracy;

                switch (clBeacon.Proximity)
                {
                    case CoreLocation.CLProximity.Far:
                        beacon.Proximity = Proximity.far;
                        break;
                    case CoreLocation.CLProximity.Near:
                        beacon.Proximity = Proximity.near;
						break;
                    case CoreLocation.CLProximity.Immediate:
                        beacon.Proximity = Proximity.immediate;
						break;
                    default:
                        beacon.Proximity = Proximity.unknown;
						break;
				}
                    
 				beacon.Calibrate();
                beacons.Add(beacon);
			}

            BeaconsSited(this, new BeaconsSitedEventArgs { BeaconSitings = beacons });

		}


		#endregion

	}

}
