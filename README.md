# README #

(Created by Rod Hemphill - July 2017)

### What you will learn ###

* Understand the basic Apple iBeacon protocol.
* Use your phones Bluetooth receiver to receive messages propagated by beacons and convert them to the iBeacon protocol.


### Start with a basic Xamarin Forms app ###

* The code in this repo will return a list of beacons currently in range. 

* This exercise starts with a basic forms app that has a list view. We've created one that you can download from the "StartingApp" folder in this repo, but you could use any Xamarin app, forms or not.

* It has a ListView of "Items" - we are going to change it to a ListView of Beacons.

Notes:	
1) Simulators generally don't support Bluetooth messages, so you'll need to test against a real phone. (There are instructions on how to dummy beacons in android simulators in the AndroidAltBeaconLibrary mentioned below, but I haven't implemented this).

2) You'll need some beacons - I use the Estimate beacons, but any that conform to the iBeacon protocol will do.

3) If you want to use the Google Eddystone beacon protocol, the code related to getting Bluetooth messages and the concepts are the same. You would need to add a new interface definition for IEddystoneBeacon rather than the iBeacon protocol used here.

### Beacon Services ###

We need two interfaces:

* IBeaconServices	- to start and stop tracking for beacons and holding the updated list of beacons
* IBeacon		- modelling an individual beacon

I created a BLEBeaconFramework set of Libraries in a SolutionsFolder that define these and implement them for iOS and Android. Add these to your project. 
I've installed in these platform specific beacon managers plugins. There are others but I use:

* On Android I use AndroidAltBeaconLibrary via NuGet.
* On iOS I use iOS's CoreBluetooth.

In addition you need to inform the user that their location will be tracked:

* In iOS Info.plist add:
	<key>NSLocationWhenInUseUsageDescription</key>
	<string>** add text here why the app is tracking their location **</string>

* ... and if you want to monitor their location when the app is running the background (note this is good candidate for Apple rejecting your app):
	<key>NSLocationAlwaysUsageDescription</key>
	<string>** add text here why tracking while app in the background (if you do this) ** </string>

* In AndroidManifest.xml:
	<uses-permission android:name="android.permission.BLUETOOTH" />
	<uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
	<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />

* ... and in the Android MainActivity:
	static readonly int REQUEST_ACCESS_COURSE_LOCATION = 1;
and
	// Handle bluetooth permissions 
	var hasAccess = this.CheckPermission(Manifest.Permission.AccessCoarseLocation, Binder.CallingPid, Binder.CallingUid);
	if (hasAccess != Permission.Granted)
	{
		var permissions = new string[] { Manifest.Permission.AccessCoarseLocation };
		this.RequestPermissions(permissions, REQUEST_ACCESS_COURSE_LOCATION);
	}


### IBeaconServices - how it works ###

Add the BLEBeaconFramework to your project. Referring to the IBeaconService interface you detect beacons by:

* Initialise the iOS and Android beacon manager - we use Initialise() for this.
* We will then want to start and stop beacon tracking at various times within our app, especially to optimise battery usage.
* The operating systems bluetooth manager checks for bluetooth messages at specific time intervals (iOS for example checks every second), and passes back a list of all beacons it sees at that time. Therefore the beacon service interface defines an event called BeaconsSited that returns a list of Beacons.
 
* You will need to add a dependency service for the IBeaconService:
	* Register in AppDelegate and MainActivity:	DependencyService.Register<IBeaconService, BeaconService>();
	* Get the created instance with:		BeaconManager = DependencyService.Get<IBeaconService>();  (in App.xaml.cs and the ViewModel) 

Get the above working first - logging console message for each beacon detected.

### Now update the ListView ###

* Within your forms projects add references to the appropriate BLEBeaconFramework projects.
* Create an ObservableCollection<IBeacon> for your ListView.
* Update the listview items to display the beacon fields.
* For beacon row you can show all beacon properties but they don't fit well. You don't necessarily need to show the UUID or Major id as these don't change.
* When you Forms app starts, initialise and start beacon monitoring.
* Listen for the BeaconsSited event.
* The BeaconsSited event returns a new list of beacons that are in range at the time rather than an updated list. 
* Update your ObservableCollection to add, remove or modify beacons.

(Application specific logic is normally required for when a beacon is first sited, goes out of range, reduces it's RSSI value less than an app specific value or increases it value above a certain value (i.e. GeoFencing) - I've left the framework very simple. If you add these to your version of the framework let me know).

You are done.

### Suggested Hacks / Improvements ###

* Instead of reloading each item in the listview create a view model for each row and have only the values update. 

* The example doesn't check whether Bluetooth is turned on. Add a cross platform implementation that checks whether bluetooth is turned on and fires an event for when it is turned on or off. Enhance your app to respond to these event.

* Rather than show beacon values, record a beacons major and minor id against an 'Item' like a painting.
	* record a specific RSSI value for that item when you consider your are close enough.
	* display the item when 'close enough'.

* Display a persons face for when he enters a coffeeshop to pick up his coffee.

* Turn on and off GPS distance tracking when a person gets in or out of his car (for a LogBook app).


