﻿using System;
using Android.OS;
using Android.Content;
using Android.Runtime;
using System.Collections.Generic;
using AltBeaconOrg.BoundBeacon;
using BLEBeaconFramework.Droid;
using BLEBeaconFramework.Interfaces;

//[assembly: Xamarin.Forms.Dependency(typeof(BeaconService))]
namespace BLEBeaconFramework.Droid
{
    public class BeaconService : Java.Lang.Object, IBeaconConsumer, IRangeNotifier, IBeaconService
	{
        private BeaconManager beaconLocationMgr { get; set; }
		private bool trackingBeacons = false; // There is a delay in starting and stopping tracking of beacons
 
		#region constructor
		public BeaconService()
		{
		}

		public void Dispose()
		{
			beaconLocationMgr.Unbind(this);
		}

		#endregion
		#region IBeaconsService Interfaces
        public void Initialise()
		{
			ApplicationContext = Android.App.Application.Context;

			beaconLocationMgr = BeaconManager.GetInstanceForApplication(ApplicationContext);
			beaconLocationMgr.BeaconParsers.Add(new BeaconParser().SetBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
			beaconLocationMgr.Bind(this);
		}

		public void StartTracking()
		{
			if (!this.trackingBeacons) // don't start twice
			{
				beaconLocationMgr.SetRangeNotifier(this);  // Tell me when beacons are in range
				try
				{
					beaconLocationMgr.StartRangingBeaconsInRegion(new Region(Settings.BeaconAppId, Identifier.Parse(Settings.Uuid), null, null));
				}
				catch (RemoteException e)
				{
					e.PrintStackTrace();
				}
				this.trackingBeacons = true;
			}
		}

		public void StopTracking()
		{
			if (this.trackingBeacons) // don't stop if not running
			{
				beaconLocationMgr.SetRangeNotifier(this);
				try
				{
					beaconLocationMgr.StopRangingBeaconsInRegion(new Region(Settings.BeaconAppId, Identifier.Parse(Settings.Uuid), null, null));
				}
				catch (RemoteException e)
				{
					e.PrintStackTrace();
				}
				this.trackingBeacons = false;
			}
		}

		public event EventHandler<BeaconsSitedEventArgs> BeaconsSited = delegate { };
        public event EventHandler BeaconServiceConnected;

        #endregion
        #region IBeaconConsumer Interfaces
        private Context _applicationContext;
		public Context ApplicationContext
		{
			get
			{
				return _applicationContext;
			}
			set { _applicationContext = value; }
		}

		public bool BindService(Intent intent, IServiceConnection serviceConnection, [GeneratedEnum] Bind flags)
		{
			return ApplicationContext.BindService(intent, serviceConnection, flags);
		}

		public void OnBeaconServiceConnect()
		{
			BeaconServiceConnected(this, new EventArgs { });
		}

		void IBeaconConsumer.UnbindService(IServiceConnection serviceConnection)
		{
			beaconLocationMgr.Unbind(this);
		}

		#endregion
		#region IRangeNotifier Interfaces
		void IRangeNotifier.DidRangeBeaconsInRegion(ICollection<AltBeaconOrg.BoundBeacon.Beacon> altBeacons, Region region)
		{
			if (!this.trackingBeacons)
				return;

			var beacons = new List<IBeacon>();

			foreach (AltBeaconOrg.BoundBeacon.Beacon altBeacon in altBeacons)
			{
                IBeacon beacon = new Beacon();

                beacon.Uuid = altBeacon.Id1.ToString();
                beacon.Major = (int)altBeacon.Id2;
                beacon.Minor = (int)altBeacon.Id3;
                beacon.Rssi = altBeacon.Rssi;
                beacon.Distance = altBeacon.Distance;

                beacon.Accuracy = 0.0; // TODO
                beacon.Proximity = Proximity.unknown;   //TODO

                beacon.Calibrate();

                beacons.Add(beacon);
			}

			BeaconsSited(this, new BeaconsSitedEventArgs { BeaconSitings = beacons });
		}

        #endregion
    }
}

