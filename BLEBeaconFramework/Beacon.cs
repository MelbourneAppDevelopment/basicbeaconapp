﻿using System;
using BLEBeaconFramework.Interfaces;

namespace BLEBeaconFramework
{
    public class Beacon: IBeacon, IDisposable
    {
        public Beacon()
        {
        }

		#region IBeacon Interface
		public string Uuid { get; set; }
		public int Minor { get; set; }
		public int Major { get; set; }
		public int Rssi { get; set; }
		public Proximity Proximity { get; set; }
		public double Distance { get; set; }
		public double Accuracy { get; set; }

		public void Calibrate()
		{
			//TODO Best practice is to calibrate each beacon to allow for individual differences.
		}
		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~Beacon() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}

		#endregion

	}
}
