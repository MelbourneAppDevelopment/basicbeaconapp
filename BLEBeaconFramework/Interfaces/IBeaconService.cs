﻿using System;
using System.Collections.Generic;

namespace BLEBeaconFramework.Interfaces
{
	public class BeaconsSitedEventArgs : EventArgs
	{
		public List<IBeacon> BeaconSitings { get; set; }
	}
	public interface IBeaconService
    {
		void Initialise();
		void StartTracking();
		void StopTracking();
		event EventHandler BeaconServiceConnected;
		event EventHandler<BeaconsSitedEventArgs> BeaconsSited; 
	}
}
